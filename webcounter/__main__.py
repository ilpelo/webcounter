from flask import Flask, render_template
from waitress import serve


from webcounter import __version__, __main__, get_hits_count

# Create a Flask APP
app = Flask(__name__)

@app.route('/')
def index(): # pragma: no cover, don't test index page
    """ Default page"""
    try:
        hits = get_hits_count()
    except:
        return f'ERR# Redis server not found'
    
    return render_template('index.html', version=__version__, hits=hits)


if __name__ == '__main__': # pragma: no cover, don't test main
    print(f'Starting webcounter v:{__version__}')
    serve(app, host='0.0.0.0', port=5000)